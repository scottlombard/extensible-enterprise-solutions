import random 

low = int(input('Enter beginning value: '))
high = int(input('Enter ending value: '))

print('\nOutput')
print('Example 1: Using range() and randint() functions:')
for x in range (1,10): print(random.randint(low, high), end = ' ')

high+=1

print('\nExample 2: Using a list, with range() and shuffle() functions:')

nums = list(range(low,high))
random.shuffle(nums)
for y in range (10): print(nums[y], end = ' ')

print("")


