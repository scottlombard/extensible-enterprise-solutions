print("Calorie Percentage")

print("input: ")
gFats = float(input("Enter total fat grams: "))
gCarbs = float(input("Enter total carb grams: "))
gProtein = float(input("Enter total protein grams: "))

fatCals = float(gFats*9)
carbCals = float(gCarbs*4)
proteinCals = float(gProtein*4)
totalCals = float(fatCals+proteinCals+carbCals)

fatPct = float((fatCals/totalCals)*100)
carbPct = float((carbCals/totalCals)*100)
proteinPct = float((proteinCals/totalCals)*100)

print("\nOutput:")
print("Type\tCalories\tPercentage")
print("Fat\t{0:,.2f}\t{1:>9.2f}%".format(fatCals,fatPct))
print("Carbs\t{0:,.2f}\t{1:>9.2f}%".format(carbCals,carbPct))
print("Protein\t{0:,.2f}\t{1:>9.2f}%".format(proteinCals,proteinPct))
