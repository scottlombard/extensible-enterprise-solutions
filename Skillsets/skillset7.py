print("\nPython Dictionaries \n \n")
print("Program Requirements:")
print("1. Dictionaries (python data strcuture): unordered key:value pairs")
print("2. Dictionary: an associative array (also known as hashes)")
print("3. Any key in a dictionary is associated (or mapped) to a value (i.e., any python data type)")
print("4. Keys: must be of immutable type (string, number, or tuple with immutable elements) and must be unique")
print("5. Values: can be any data type and can repeat")
print("6. Create a program that mirrors the following IPO (input/process/output) format.")
print("\t Create empty dictionary, using curly braces {}: my dictionay = {}")
print("\t Use the following keys: fname, lname, degree, major, gpa")
print("Dictionaries have key-valye pairs instead of single values; this differentiates a dictionary from a set.")

print ("Input:")
fname = input("Enter first name: \t")
lname = input("Enter last name: \t")
degree = input("Enter degree: \t\t")
major = input("Enter major: \t\t")
gpa = float(input("Enter GPA: \t\t"))

my_dictionary =	{
  "fname": fname,
  "lname": lname,
  "degree": degree,
  "major": major,
  "gpa": gpa
}

print ("\nOutput:")
print("Print my_dictionary", my_dictionary)
print("\nReturn view of dictionary's (key, value) pair, built-in function: ", my_dictionary.items())
print("\nReturn view object of all keys, built-in function: ", my_dictionary.keys())
print("\nReturn view object of all values, built-in function: ", my_dictionary.values())
print("\nPrint only first and last names, using keys: \n", my_dictionary['fname'], my_dictionary['lname'])
print("\nPrint only first and last names, using get(): ") 
print(my_dictionary.get('fname'), my_dictionary.get('lname'))
print("\nCount number of items (key:value pairs) in dictionary: ")
print(len(my_dictionary))
print("\nRemove last dictionary item (popitem):")
my_dictionary.popitem()
print(my_dictionary)
print("\nDelete major from dictionary, using key:")
del my_dictionary['major']
print(my_dictionary)
print('\nReturn object type:')
print(type(my_dictionary))
print('\nDelete all items from list:')
my_dictionary.clear()
print(my_dictionary)


