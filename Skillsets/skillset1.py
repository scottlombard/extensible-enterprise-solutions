print ("Square Feet to Acres\n")

print ('Input:')
sqFeet = float(input('Enter Square Feet: '))

acres = float(sqFeet/43560)

print ('Output:')
print("{0:,.2f} square feet = {1:.2f} acres".format(sqFeet,acres))
