# LIS4369 - Extensible Enterprise Solutions

## Scott Lombard

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links
	a) This assignment
	b) the completed tutorial (bitbucketstationlocations)
#### README.md file should include the following items:

![VSC Screenshot](images/A1_VSC.PNG)
> #### Git commands w/short descriptions:

1. git init - Creates an empty git repository.
2. git status - Displays the state of a working directory.
3. git add - This stages files so that they can be committed.
4. git commit - Record changes to a repository.
5. git push - Updates remote repository with committed changes.
6. git pull - Updates your local repository with changes  from a remote repository.
7. git log - View changes to your repositories.

#### Assignment Screenshots:

*Screenshot of a1_tip_calculator running (IDLE)*:
![IDLE Screenshot](images/A1_IDLE.PNG)

##### Tutorial Repo:
*https://bitbucket.org/scottlombard/bitbucketstationlocations/src/master/