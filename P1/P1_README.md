# LIS4369 - Extensible Enterprise Solutions

## Scott Lombard

### Project 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Questions
3. Bitbucket repo links
	a) This assignment

#### Assignment Screenshots:
*Screenshot of demo.py(Visual Studio Code)*:
![See images folder](images/Part1.PNG)
![See images folder](images/Part2.PNG)
![See images folder](images/Part3.PNG)

##### Repo:
*https://bitbucket.org/scottlombard/bitbucketstationlocations/src/master/