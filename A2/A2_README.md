# LIS4369 - Extensible Enterprise Solutions

## Scott Lombard

### Assignment 2 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Questions
3. Bitbucket repo links
	a) This assignment

#### Assignment Screenshots:
*Screenshot of a2_payroll calculator(Visual Studio Code)*:
![See images folder](images/A2_vscode.PNG)

*Screenshot of a2_payroll calculator(IDLE)*:
![See images folder](images/A2_idle.PNG)

##### Repo:
*https://bitbucket.org/scottlombard/bitbucketstationlocations/src/master/