print('Payroll Calculator\n')

def get_requirements():
    print('Program Requirements:')
    print('1. Calculate home interior cost (w/o primer).')
    print('2. Must use float data types.')
    print('3. Must use SQFT_PER_GALLON constant (350).')
    print('4. Must use iteration structure (loop).')
    print('5. Format, right-align numbers, and round to two decimal places')
    print('6. Create at least three functions that are called by the program:')
    print('\ta. main(): calls at least two other functions.')
    print('\tb. get_requirements(): displays the program requirements.')
    print('\tc. estimate_painting_cost(): calculates interior home painting.')

def estimate_painting_cost():

    checker = True

    while checker == True:
        print('\nInput:')
        int_sqft = float(input('Enter total interior sq ft: '))
        ppg = float(input('Enter price per gallon: '))
        hourly_rate = float(input('Enter hourly painting rate per sq ft: '))
        num_gallons = float(int_sqft/350)

        print('\nOutput:')
        print('Item\t\t\tAmount')
        print('Total Sq Ft:\t{0:>14,.2f}'.format(int_sqft))
        print('Sq Ft Per Gallon:       350.00')
        print('Number of Gallons:\t{0:>6,.2f}'.format(num_gallons))
        print('Price Per Gallon:    ${0:>8,.2f}'.format(ppg))
        print('Labor Per Sq Ft:     ${0:>8,.2f}'.format(hourly_rate))

        paint_cost = float(ppg*num_gallons)
        labor_cost = float(hourly_rate*int_sqft)
        total_cost = float(paint_cost+labor_cost)

        paint_perc = float((paint_cost/total_cost)*100)
        labor_perc = float((labor_cost/total_cost)*100)
        total_perc = float(labor_perc+paint_perc)

        print('\nCost:\t     Amount\tPercentage')
        print('Paint: \t  $  {0:,.2f}\t{1:>9,.2f}%'.format(paint_cost,paint_perc))
        print('Labor: \t  ${0:,.2f}\t{1:>9,.2f}%'.format(labor_cost,labor_perc))
        print('Total: \t  ${0:,.2f}\t{1:>9,.2f}%'.format(total_cost,total_perc))

        y = str('y')
        n = str('n')
        again = str(input('Estimate another paint job? (y/n): '))
        if again == 'y':
            checker = True
        elif again == 'n':
            checker = False
        
    print('Thank you for using our Painting Estimator!')
    print('Please see our web site: https://scottalombard.com')        

def main():
    get_requirements()
    estimate_painting_cost()

main()


