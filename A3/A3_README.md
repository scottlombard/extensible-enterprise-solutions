# LIS4369 - Extensible Enterprise Solutions

## Scott Lombard

### Assignment 3 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Questions
3. Bitbucket repo links
	a) This assignment

#### Assignment Screenshots:
*Screenshot of a3_payroll calculator(Visual Studio Code)*:
![See images folder](images/A3_vscode.PNG)

*Screenshot of a3_payroll calculator(IDLE)*:
![See images folder](images/A3_idle.PNG)

##### Repo:
*https://bitbucket.org/scottlombard/bitbucketstationlocations/src/master/